//! Initialization code

#![no_std]

// pub use stm32f3_discovery::{leds::Leds, stm32f3xx_hal, switch_hal};
pub use switch_hal::{ActiveHigh, OutputSwitch, Switch, ToggleableOutputSwitch, IntoSwitch};

pub use panic_itm; // panic handler

use stm32f4xx_hal::prelude::*;
pub use cortex_m_rt::entry;

pub use stm32f4xx_hal::{
    gpio::{gpiod, Output, PushPull},
    delay::Delay,
    pac,
    rcc::RccExt,
    hal::blocking::delay::DelayMs,
};


pub type LedArray = [Switch<gpiod::PDn<Output<PushPull>>, ActiveHigh>; 4];

type Led = Switch<gpiod::PDn<Output<PushPull>>, ActiveHigh>;

pub struct Leds {
    pub ld3: Led,
    pub ld4: Led,
    pub ld5: Led,
    pub ld6: Led,
}

impl Leds {
    /// Initializes the user LEDs to OFF
    pub fn new<PD12Mode, PD13Mode, PD14Mode, PD15Mode>(
        pd12: gpiod::PD12<PD12Mode>,
        pd13: gpiod::PD13<PD13Mode>,
        pd14: gpiod::PD14<PD14Mode>,
        pd15: gpiod::PD15<PD15Mode>,
    ) -> Self {
        let mut leds = Leds {
            ld3: pd13
                .into_push_pull_output()
                .erase_number()
                .into_active_high_switch(),
            ld4: pd12
                .into_push_pull_output()
                .erase_number()
                .into_active_high_switch(),
            ld5: pd14
                .into_push_pull_output()
                .erase_number()
                .into_active_high_switch(),
            ld6: pd15
                .into_push_pull_output()
                .erase_number()
                .into_active_high_switch(),
        };

        for led in &mut leds {
            led.off().ok();
        }

        leds
    }
    pub fn into_array(self) -> [Led; 4] {
        [
            self.ld3,
            self.ld5,
            self.ld4,
            self.ld6,
        ]
    }

    pub fn iter_mut(&mut self) -> LedsMutIterator {
        LedsMutIterator::new(self)
    }
}

impl<'a> IntoIterator for &'a mut Leds {
    type Item = &'a mut Led;
    type IntoIter = LedsMutIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

const ITERATOR_SIZE: usize = 4;

pub struct LedsMutIterator<'a> {
    index: usize,
    index_back: usize,
    leds: &'a mut Leds
}

impl<'a> LedsMutIterator<'a> {
    fn new(leds: &'a mut Leds) -> Self {
        LedsMutIterator { index: 0, index_back: ITERATOR_SIZE, leds }
    }

    fn len(&self) -> usize {
        self.index_back - self.index
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let length = self.len();
        (length, Some(length))
    }
}

impl<'a> Iterator for LedsMutIterator<'a> {
    type Item = &'a mut Led;
    fn next(&mut self) -> Option<Self::Item> {
        if self.len() == 0 {
            None
        } else {
            let current = unsafe {
                //Safety: Each branch is only executed once,
                // and only if there are elements left to be returned, 
                // so we can not possibly alias a mutable reference.
                // This depends on DoubleEndedIterator and ExactSizedIterator being implemented correctly.
                // If len() does not return the correct number of remaining elements, 
                // this becomes unsound.
                    match self.index {
                        0 => Some(&mut *(&mut self.leds.ld3 as *mut _)),  //N
                        1 => Some(&mut *(&mut self.leds.ld4 as *mut _)),  //NE
                        2 => Some(&mut *(&mut self.leds.ld5 as *mut _)),  //E
                        3 => Some(&mut *(&mut self.leds.ld6 as *mut _)),  //SE
                        _ => None
                }
            };
            self.index += 1;
            current
        }
    }

    // Because we implement ExactSizedIterator, we need to ensure size_hint returns the right length
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.size_hint()
    }
}

pub fn init() -> (Delay, LedArray) {
    let device_periphs = pac::Peripherals::take().unwrap();
    // let device_periphs2 = pac::Peripherals::take().unwrap();
    let reset_and_clock_control = device_periphs.RCC.constrain();

    let core_periphs = cortex_m::Peripherals::take().unwrap();
    let clocks = reset_and_clock_control.cfgr.freeze();
    let delay = Delay::new(core_periphs.SYST, &clocks);

    let gpiod = device_periphs.GPIOD.split();
    let leds = Leds::new(
        gpiod.pd12,
        gpiod.pd13,
        gpiod.pd14,
        gpiod.pd15,
    );

    (delay, leds.into_array())
}

